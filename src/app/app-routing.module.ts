import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ScrumboardComponent } from './scrumboard/scrumboard.component';


const routes: Routes = [
	{path : '', component : HomepageComponent},
	{path : 'home', component : HomepageComponent},
	{path : 'login', component : LoginComponent},
	{path : 'signup', component : SignupComponent},
	{path : 'scrumboard', component : ScrumboardComponent}

	];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
