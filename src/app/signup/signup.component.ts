import { Component, OnInit } from '@angular/core';
import { Scrumuser } from '../scrumuser';
import { ScrumdataService } from '../scrumdata.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private _scrumdataService: ScrumdataService){}

  ngOnInit() {
  }
	usertype = ['Developer', 'Owner'];
	scrumUserModel = new Scrumuser('', '', '', '', '');
	feedbk = '';
	  
	onSubmit() {
		console.log(this.scrumUserModel);
		this._scrumdataService.signup(this.scrumUserModel).subscribe(
		
		data => {
			console.log('Success!', data)
			this.feedbk = '😊 Your account has been successfully created'
		},
		error => {
			console.error('Error!', error)
			this.feedbk = '😟 Sorry, Your signup failed'
		}
		)

		}
	
}	